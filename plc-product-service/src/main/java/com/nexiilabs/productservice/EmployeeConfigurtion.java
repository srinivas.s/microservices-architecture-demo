package com.nexiilabs.productservice;

import java.util.ArrayList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EmployeeConfigurtion {

	@Bean
	public EmployeeService getEmployeeServiceObj() {
		return new EmployeeService();
	}
	
	@Bean
	public ArrayList<EmployeeService> getEmployeeService() {
		return new ArrayList<EmployeeService>();
	}
}
