package com.nexiilabs.jiraintegration;

import java.util.ArrayList;
import java.util.List;

public class EmployeeService {
	int requestNumber=0;
	public EmployeeService() {
		
	}

	public List<Employee> getAllEmployees(){
		
		List<Employee> list=new ArrayList<Employee>();
		
		list.add(new Employee(1,"RAMA", requestNumber++));
		list.add(new Employee(2,"HARI",requestNumber++));
		list.add(new Employee(3,"KRISHNA", requestNumber++));
		list.add(new Employee(4,"GOVINDA", requestNumber++));
		
		return list;
	}
}
