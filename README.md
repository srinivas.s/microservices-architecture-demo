# microservices-architecture-demo
This is a sample demo application on microservices which covers service discover using eureka server, api gateway using zoolproxy, log analsys using zipkin, monitoring dashboard using hystrix, load balancing using ribbon and rest service client

# microservices-architecture-demo setup
*  save the source code by cloning or downloading the zip file
*  go to the directory where you have downloded or cloned then import all the projects as maven projects in eclipse
*  now build using maven 
*  run all the utility apps in the order of service discovery, api gateway, log analysis, monitoring dashboard
*  now run remining projects like plc admin, plc features, plc file resource, plc jira integration, plc product, plc releases and plc service client

# Service Discovery: Eureka Server testing
*  eureka server can be acceesed on this url http://localhost:8761/, it will show all the services up and running

# Monitoring Dashboard: Hystrix
*  open following link in browser http://localhost:9981/hystrix
*  now paste following link http://localhost:9981/turbine.stream in hystrix dashboard input field then click on monitor stream
*  UI will show the failured API calls, Timeout API calls

# Application Tracing/Log analysis: ZIPKIN
*  open following link in browser http://localhost:9411/zipkin/
*  here we can see the rest call routing and dependency analysation

# Accesing apllication services through API Gateway
*  PLC-ADMIN-SERVICE service running on 9982 port and sample REST API is http://localhost:9980/api/PLC-ADMIN-SERVICE/getEmployees
*  PLC-FEATURES-SERVICE service running on 9983 port and sample REST API is http://localhost:9980/api/PLC-FEATURES-SERVICE/getEmployees
*  PLC-PRODUCT-SERVICE service running on 9986 port and sample REST API is http://localhost:9980/api/PLC-PRODUCT-SERVICE/getEmployees
*  PLC-RELEASES-SERVICE service running on 9987 port and sample REST API is http://localhost:9980/api/PLC-RELEASES-SERVICE/getEmployees
*  PLC-SERVICE-CLIENT service running on 9988 port and sample REST API is http://localhost:9980/api/PLC-SERVICE-CLIENT/showData
*  PLC-FILE-RESOURCE-SERVICE service running on 9984 port and sample REST API is http://localhost:9980/api/PLC-FILE-RESOURCE-SERVICE/getEmployees
*  PLC-JIRA-INTEGRATION-SERVICE service running on 9985 port and sample REST API is http://localhost:9980/api/PLC-JIRA-INTEGRATION-SERVICE/getEmployees

# Load balancing Check:
*  took any of the service build project, for example PLC-ADMIN-SERVICE
*  go to application.prperties change the port number and run it again
*  now PLC-ADMIN-SERVICE service load balanced with two servives, you can found it in service discovery url.
*  for load balance verification, first hit the admin service 3 times you can find request id as 12, hit again after starting the load balance application again you can find the requirement id as started with 1