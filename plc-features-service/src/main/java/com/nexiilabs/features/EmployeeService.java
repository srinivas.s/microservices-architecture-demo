package com.nexiilabs.features;

import java.util.ArrayList;
import java.util.List;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

public class EmployeeService {
	int requestNumber=0;
	public EmployeeService() {
		
	}

	@HystrixCommand(fallbackMethod="getAllEmployeesFallBack")
	public List<Employee> getAllEmployees(){
		
		List<Employee> list=new ArrayList<Employee>();
		
		list.add(new Employee(1,"RAMA", requestNumber++));
		list.add(new Employee(2,"HARI",requestNumber++));
		list.add(new Employee(3,"KRISHNA", requestNumber++));
		list.add(new Employee(4,"GOVINDA", requestNumber++));
		
		return list;
	}
	
	public List<Employee> getAllEmployeesFallBack(Throwable t) {
		System.err.println("From Hystrix Fall back method ::: getAllEmployeesFallBack() ");
		return new ArrayList<Employee>();
	}
}
