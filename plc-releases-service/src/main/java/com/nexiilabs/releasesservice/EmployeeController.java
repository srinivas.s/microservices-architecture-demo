package com.nexiilabs.releasesservice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController {
	EmployeeService employeeService;

	public EmployeeController(@Autowired EmployeeService employeeService) {
		this.employeeService=employeeService;
	}
	
	@RequestMapping("/getEmployees")
	private List<Employee> getEmployees(){
		return employeeService.getAllEmployees();
	}
}
