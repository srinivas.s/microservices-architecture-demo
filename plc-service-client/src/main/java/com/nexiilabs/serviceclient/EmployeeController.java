package com.nexiilabs.serviceclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController {
	
	@Autowired
	public DataProviderService dataProviuder;
	
	@RequestMapping("/showData")
	private String getEmployees(){
		return dataProviuder.getDataToShow();
	}
}
