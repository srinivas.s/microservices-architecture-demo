package com.nexiilabs.serviceclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Service
public class DataProviderService {
	
	@Autowired
	public RestTemplate restTemplate;
	
	@HystrixCommand(fallbackMethod = "getDataToShowFallBack")
	public String getDataToShow() {
		ResponseEntity<String> resp=restTemplate.exchange("http://PLC-ADMIN-SERVICE/getEmployees", HttpMethod.GET, null, String.class);
		return resp.getBody();
	}
	
	public String getDataToShowFallBack(Throwable t) {
		System.err.println("From Hystrix Fall back method ::: getDataToShowFallBack() ");
		return "{statusCode: 0, message: Service Went down}";
	}

}
