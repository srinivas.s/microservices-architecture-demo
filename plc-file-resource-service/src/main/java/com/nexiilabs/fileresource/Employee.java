package com.nexiilabs.fileresource;

public class Employee {
	
	private int empId;
	private String empName;
	private int requestId;
	
	public Employee() {
		
	}
	public Employee(int empId,String empName, int requestId) {
		this.empId=empId;
		this.empName=empName;
		this.requestId=requestId;
	}
	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public int getRequestId() {
		return requestId;
	}
	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

}
